import kotlinx.coroutines.*

fun main(args: Array<String>) = runBlocking<Unit> {
    val job = launch {
        try {
            repeat(3) { i ->
                println("I'm sleeping $i ...")
                delay(500L)
            }
        } finally {
            println("I'm running finally")
        }
    }
    delay(1300L)
    println("main: I'm tired of waiting!")
    job.cancelAndJoin()
    println("main: Now I can quit.")
}
