import kotlinx.coroutines.*

suspend fun func1():Int{
    delay(500L)
    return 3
}

suspend fun func2():Int{
    delay(500L)
    return 7
}

suspend fun main()= coroutineScope{
    var t1= System.currentTimeMillis()
    println(func1()+func2())
    var t2= System.currentTimeMillis()
    println("Time: ${t2-t1}")
    t1= System.currentTimeMillis()
    val numDef1=async{ func1() }
    val numDef2=async{ func2() }
    println(numDef1.await()+numDef2.await())
    t2= System.currentTimeMillis()
    println("Time: ${t2-t1}")
    /*В первом случае время вызова будет в 2 раза больше, чем во втором, т.к. при последовательном вызове
    функции будут вызываться одна за другой, а при асинхронном они будут выполняться параллельно*/
}
